USE worlddb;

CREATE TABLE roles(
idRole INT,
nom VARCHAR(25),
permission VARCHAR(6),
PRIMARY KEY (idRole)
);

CREATE TABLE utilisateur (
  id int,
  login varchar(50),
  password varchar(100),
  nom varchar(50),
  idrole int,
  PRIMARY KEY(id),
  FOREIGN KEY(idrole) REFERENCES roles(idRole)
);
