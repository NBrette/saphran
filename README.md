# WorldDB

Application web permettant la visualisation des données sur les pays et villes du monde

## Installation

- Cloner le projet
```bash
git clone https://gitlab.com/NBrette/saphran.git
```

- Injecter les script SQL:

```bash
mysql -p -u user < saphran/sql/WorldDBWithFlagUTF8.sql
mysql -p -u user < saphran/sql/UserRole.sql
```
- Exécuter le Faker pour insérer des utilisateurs et des rôles
```bash
cd saphran/src/utils
php -f Faker.php
```
- Modifier les paramètres du fichier de configuration pour la connexion à la BDD
## Utilisation
Exécution d'un serveur PHP:
```bash
php -S127.0.0.1:8080 -t ../../public_html
```
Il y a trois utilisateurs permettant de tester l'application ayant des permissions différentes:
- **admin@world.com**
- **dev@world.com**
- **testeur@world.com**

Tous ont le même mot de passe: **123+Aze**