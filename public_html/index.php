<?php

require_once '../src/controllers/DefaultController.php';
require_once '../src/controllers/CityController.php';
require_once '../src/controllers/CountryController.php';
require_once '../src/controllers/UserController.php';

if (isset($_SERVER["PATH_INFO"])) {
    $path = trim($_SERVER["PATH_INFO"], "/");
} else {
    $path = "";
}

$fragments = explode("/", $path);

//var_dump($fragment);

$control = array_shift($fragments);

switch ($control) {
    case "city" : {
            //calling function to prevend all hard code here
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                cityRoutes_get($fragments);
            }
            else if ($_SERVER["REQUEST_METHOD"] == "POST") {
                cityRoutes_post($fragments);
            }
            break;
        }
    // /country
    case "country" : {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                countryRoutes_get($fragments);
            }
            else if ($_SERVER["REQUEST_METHOD"] == "POST") {
                countryRoutes_post($fragments);
            }
            break;
        }
    case "login" : {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                loginRoutes_get($fragments);
            }
            else if ($_SERVER["REQUEST_METHOD"] == "POST") {
                loginRoutes_post($fragments);
            }
            break;
        }
    case "logout" : {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                logoutRoutes_get($fragments);
            }
            break;
        }
    // /
    // route par défaut
    default : {
            $defaultcontroller = new DefaultController();
            $defaultcontroller->show();
            break;
        }
}

function cityRoutes_get($fragments) {
    
    $control = array_shift($fragments);
    
    //      /city/edit/id
    if ($control == 'edit') 
    {
        call_user_func_array([new CityController(), "edit"], $fragments);
    }
    else if($control == 'add') 
    {
        call_user_func_array([new CityController(), "save"], $fragments);
    }
    else if($control == 'del') 
    {
        call_user_func_array([new CityController(), "delete"], $fragments);
    }
    
}

function cityRoutes_post($fragments) {
    
    $control = array_shift($fragments);
    
    //      /city/edit
    //Route sur laquelle est envoyé le formulaire de modif des villes
    if ($control == 'edit') 
    {
        call_user_func_array([new CityController(), "doEdit"], $fragments);
    }
    else if ($control == 'add') 
    {
        call_user_func_array([new CityController(), "doSave"], $fragments);
    }
    
}

function loginRoutes_post($fragments) {
    call_user_func_array([new UserController(), "checkConnexion"], $fragments);

}

function loginRoutes_get($fragments) {
    call_user_func_array([new UserController(), "showForm"], $fragments);  
}

function logoutRoutes_get($fragments) {
    call_user_func_array([new UserController(), "logout"], $fragments);  
}

function countryRoutes_post($fragments) {
    
    $control = array_shift($fragments);
    
    //      /city/edit
    //Route sur laquelle est envoyé le formulaire de modif des villes
    if ($control == 'edit') 
    {
        call_user_func_array([new CountryController(), "doEdit"], $fragments);
    }
    else if ($control == 'add') 
    {
        echo 'appel dosave';
        call_user_func_array([new CountryController(), "doSave"], $fragments);
    }
    
}



function countryRoutes_get($fragments) {

    
    $control = array_shift($fragments);
    
    //      /country/add
    if ($control == 'add')
    {
        call_user_func_array([new CountryController(), "save"], [$control]);
    }
    //      /country/edit
    else if ($control == 'edit')
    {
        call_user_func_array([new CountryController(), "edit"], $fragments);
    }
    else if($control == 'del') 
    {
        call_user_func_array([new CountryController(), "delete"], $fragments);
    }
    //      /country/id
    else if (ctype_digit($control)=='integer')
    {
        call_user_func_array([new CountryController(), "show"], [$control]);
    }
    //      /country/continent
    else if (gettype($control) == 'string')
    {
        call_user_func_array([new CountryController(), "showAllByContinent"], [$control]);
    }
    //      /country
    else if($control == null)
    {
        call_user_func_array([new CountryController(), "showAll"], $fragments);
    }

    
}


?>





