<?php

require_once 'BaseController.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/utils/SingletonDatabase.php';
require_once '../src/utils/Renderer.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DefaultController
 *
 * @author nicolas
 */
class DefaultController extends BaseController{
    
    /** @var DAOCountry $daocountry*/
    private $daocountry;
    
        public function __construct() 
    {
        $this->daocountry = new DAOCountry(SingletonDatabase::getInstance()->cnx);
    }
    
    public function show()
    {
        $continent = $this->daocountry->get_enum_from_continent();
        $page = renderer::render('welcome.php',compact('continent'));
        echo $page;
    }
    
}
