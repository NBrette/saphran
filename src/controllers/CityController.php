<?php

require_once 'BaseController.php';
require_once '../src/utils/CsrfToken.php';
require_once '../src/utils/Filter.php';
require_once '../src/utils/CountryCodeVisitor.php';
require_once '../src/utils/PopulationVisitor.php';
require_once '../src/utils/NameVisitor.php';
require_once '../src/utils/DistrictVisitor.php';


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CityController
 *
 * @author student
 */
class CityController extends BaseController  {
    
    use CsrfToken;
    
    /** @var DAOCity $city*/
    private $daocity;
            
    public function __construct() 
    {
        $this->daocity = new DAOCity(SingletonDatabase::getInstance()->cnx);
    }
    

    public function show($id) 
    {

    }
    

        /**
     * 
     * @param type $id id du pays à editer
     * Affiche la page de tous les pays
     */
    public function edit($id)
    {
        
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        
        if (Auth::isLogged() && Auth::isAllowed(Auth::$CANUPDATE)){
            $csrf_token = $this->generateToken();

            $ville = $this->daocity->find($id);
            $page = renderer::render('editcity.php',compact('ville','csrf_token'));
            echo $page;
        }
    }
    
    /**
     * fonction qui modifie une ville avec les données du formulaire
     */
    public function doEdit()
    {
        /** @var City $villeToEdit */
        
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        
        if ($this->check('csrf_token') && Auth::isLogged() && Auth::isAllowed(Auth::$CANUPDATE))
        {
            $id = htmlspecialchars($_POST['id']);
            $nom = htmlspecialchars($_POST['nom']);
            $district = htmlspecialchars($_POST['district']);
            $pop = htmlspecialchars($_POST['population']);
            $code = htmlspecialchars($_POST['code']);

            $villeToEdit = $this->daocity->find($id);
            $villeToEdit->setName($nom);
            $villeToEdit->setDistrict($district);
            $villeToEdit->setCountryCode($code);
            $villeToEdit->setPopulation($pop);


            
            $this->daocity->update($villeToEdit);
            
            header('Location:/city/edit/'.$id);
        }
        else
        {
            $_SESSION['flash'] = "Erreur durant la modification de la ville";
            $url = $_SERVER['HTTP_REFERER'];
            header('Location:'.$url);
        }
        
         
         
        
        
        
    }
    
    /**
     * Affiche le formulaire d'ajout de ville
     */
    public function save()
    {
        
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        
        if (Auth::isLogged() && Auth::isAllowed(Auth::$CANCREATE)){
            $csrf_token = $this->generateToken();
            $page = renderer::render('addcity.php',compact('csrf_token'));
            echo $page;
        }
    }
    
    /**
     * Enregistre une ville créé via un formulaire
     */
    public function doSave()
    {
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }

        if ($this->check('csrf_token') && Auth::isLogged() && Auth::isAllowed(Auth::$CANCREATE))
        {
            
            $nom = htmlspecialchars($_POST['nom']);
            $district = htmlspecialchars($_POST['district']);
            $pop = htmlspecialchars($_POST['population']);
            $code = htmlspecialchars($_POST['code']);
            
            $data = ['Population'=>$pop,'CountryCode'=>$code,'Nom'=>$nom,'District'=>$district];
            $filtre = new Filter($data);
            $filtre ->acceptVisitor('CountryCode',new CountryCodeVisitor());
            $filtre ->acceptVisitor('Population',new PopulationVisitor());
            $filtre ->acceptVisitor('Nom',new NameVisitor());
            $filtre ->acceptVisitor('District',new DistrictVisitor());
            
            
            if ($filtre->visit($data))
            {
                $ville = new City();
                $ville->setDistrict($district);
                $ville->setName($nom);
                $ville->setPopulation($pop);
                $ville->setCountryCode($code);

                $this->daocity->save($ville);

                header('Location:/city/add');
            }
            else
            {
                $_SESSION['flash'] = "Erreur durant l'ajout de la ville";
                $url = $_SERVER['HTTP_REFERER'];
                header('Location:'.$url);
            }


        }
        else
        {
            $_SESSION['flash'] = "Erreur durant l'ajout de la ville";
            $url = $_SERVER['HTTP_REFERER'];
            header('Location:'.$url);
        }
    }
    
    /**
     * 
     * @param type $id id de la ville à supprimer
     * Supprime une ville
     */
    public function delete($id)
    {
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        
        if (Auth::isLogged() && Auth::isAllowed(Auth::$CANDELETE)){
            /** @var City $ville */
            $ville = $this->daocity->find((int)$id);
            echo $ville->getName();
            $this->daocity->remove($ville);
            $url = $_SERVER['HTTP_REFERER'];
            echo $url;

            header('Location:'.$url);
        }
    }
}
