<?php
require_once 'BaseController.php';
require_once '../src/utils/CsrfToken.php';
require_once '../src/utils/Renderer.php';
require_once '../src/utils/PasswordVisitor.php';
require_once '../src/utils/LoginVisitor.php';
require_once '../src/model/DAOUser.php';
require_once '../src/model/DAORole.php';
require_once '../src/utils/Auth.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author student
 */
class UserController extends BaseController{
   use CsrfToken;
   
   private $daouser;
   private $daorole;
   
   public function __construct() 
   {
        $this->daouser = new DAOUser(SingletonDatabase::getInstance()->cnx);
        $this->daorole = new DAORole(SingletonDatabase::getInstance()->cnx);
   }
   
   public function showForm(){
       
       $csrf_token = $this->generateToken();
       
       $page = Renderer::render("login.php", compact('csrf_token'));
       echo $page;
       
   }
   
   /**
    * Vérifie les identifiants de connexion passés au formulaire
    */
   public function CheckConnexion(){
       
       if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        
       if ($this->check('csrf_token'))
       {
           echo 'token ok';
           $login = htmlspecialchars($_POST['login']);
           $password = htmlspecialchars($_POST['password']);
           
           $data = ['password'=>$password,'login'=>$login];
           
           $filtre = new Filter($data);
           $filtre ->acceptVisitor('password',new PasswordVisitor());
           $filtre ->acceptVisitor('login',new LoginVisitor());
           if ($filtre->visit($data))
           {
               echo 'filtre ok';
                $user = $this->daouser->find($login,$password);
                echo 'user trouvé';
                if ($user == null)
                {
                    $_SESSION['flash'] = "Login/Mot de passe incorrect";
                    $url = $_SERVER['HTTP_REFERER'];
                    header('Location:'.$url);
                }
                else 
                {

                    $role = $this->daorole->find($user->getIdRole());
                    $user->setRole($role);
                    Auth::login($user);
                    header('Location:/');
                    

                }

                
                    
            }
            else
            {
                $_SESSION['flash'] = "Login/Mot de passe incorrect";
                $url = $_SERVER['HTTP_REFERER'];
                header('Location:'.$url);
            }


       }
       else
       {
            $_SESSION['flash'] = "Login/Mot de passe incorrect";
            $url = $_SERVER['HTTP_REFERER'];
            header('Location:'.$url);
       }
       
       
       
   }
   
   /**
    * Déconnecte l'utilisateur en remettant l'utilisateur en session à null
    */
   public function logout()
   {
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        echo 'debut logout';
        Auth::logout();
        $url = $_SERVER['HTTP_REFERER'];
        echo $url;
        header('Location:'.$url);
       
   }
   
   
}
