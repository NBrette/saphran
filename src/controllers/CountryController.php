<?php

require_once 'BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/utils/Paginator.php';
require_once '../src/utils/Renderer.php';
require_once '../src/utils/Auth.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CountryController
 *
 * @author student
 */
class CountryController extends BaseController {
    /** @var DaoCountry  $daoCountry */
    private $daocountry;
    private $daocity;
    
    use paginator;
    use CsrfToken;

    public function __construct() {
        $this->daocountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);
        
        $this->daocity = new DAOCity(SingletonDataBase::getInstance()->cnx);
    }
    
    /**
     * 
     * @param type $id id du pays à montrer
     * Affiche les détails d'un pays
     */
    public function show($id) {
        //VERIFIER LA VALIDITER DE ID
        $country = $this->daocountry->find($id);
        //$cities = $this->daocity->findCitiesByCountryCode($country->getCode());
        
         //Vérifie si des paramètres ont été passé dans l'url
        if (isset($_GET['p'])) $page = $_GET['p'] ; else $page = 1;
        if (isset($_GET['items'])) $items_per_page = $_GET['items']; else $items_per_page = 10;
        
        //Récupere le path de l'url sans les paramètres
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        
        $cities = $this->daocity->findCitiesByCountryCode($country->getCode(), $items_per_page, $page*$items_per_page-$items_per_page);
        $total_items = $this->daocity->foundRows();
        

        $pagination = $this->paginate($uri, $page, $total_items, $items_per_page);
        
        $page = Renderer::render("details.php", compact('country','cities','pagination'));
        echo $page;
    }
    
    /**
     * Affiche tous les pays
     */
    public function showAll() 
    {
        
        //Vérifie si des paramètres ont été passé dans l'url
        if (isset($_GET['p'])) $page = $_GET['p'] ; else $page = 1;
        if (isset($_GET['items'])) $items_per_page = $_GET['items']; else $items_per_page = 10;
        
        //Récupere le path de l'url sans les paramètres
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        
        $countries = $this->daocountry->findAll($items_per_page, $page*$items_per_page-$items_per_page);
        $total_items = $this->daocountry->foundRows();
        

        $pagination = $this->paginate($uri, $page, $total_items, $items_per_page);
        $page = Renderer::render("pays.php", compact('countries','pagination'));
        echo $page;
    }
    
    /**
     * 
     * @param type $continent continent à afficher
     * Affiche tous les pays d'un continent
     */
    public function showAllByContinent($continent) 
    {
        //Vérifie si des paramètres ont été passé dans l'url
        if (isset($_GET['items'])) $page = $_GET['p'] ; else $page = 1;
        if (isset($_GET['items'])) $items_per_page = $_GET['items']; else $items_per_page = 10;
        
        //Récupere le path de l'url sans les paramètres
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        
        $countries = $this->daocountry->findAllByContinent($continent,$items_per_page, $page*$items_per_page-$items_per_page);
        $total_items = $this->daocountry->foundRows();
        

        $pagination = $this->paginate($uri, $page, $total_items, $items_per_page);
        $page = Renderer::render("pays.php", compact('countries','pagination'));
        echo $page;
    }
    
    /**
     * Affiche le formulaire d'ajout de pays
     */
    public function save()
    {
        
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        
        if (Auth::isLogged() && Auth::isAllowed(Auth::$CANCREATE)){
            $csrf_token = $this->generateToken();
            $page = renderer::render('addcountry.php',compact('csrf_token'));
            echo $page;
        }

    }
    
    /**
     * Enregistre le pays créé via le formulaire
     */
    public function doSave()
    {
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        if ($this->check('csrf_token') && Auth::isLogged() && Auth::isAllowed(Auth::$CANCREATE))
        {
            
            $nom = htmlspecialchars($_POST['nom']);
            $code = htmlspecialchars($_POST['code']);
            $continent = htmlspecialchars($_POST['continent']);
            $region = htmlspecialchars($_POST['region']);
            $surface = htmlspecialchars($_POST['surface']);
            $inde = htmlspecialchars($_POST['inde']);
            $esperance = htmlspecialchars($_POST['esperance']);
            $gnp = htmlspecialchars($_POST['gnp']);
            $gnpold = htmlspecialchars($_POST['gnpold']);
            $local = htmlspecialchars($_POST['local']);
            $gov = htmlspecialchars($_POST['gov']);
            $head = htmlspecialchars($_POST['head']);
            $capital = htmlspecialchars($_POST['capital']);
            $pop = htmlspecialchars($_POST['pop']);
            $code2 = htmlspecialchars($_POST['code2']);
            $pays = new Country();
            $pays->setName($nom);
            $pays->setCode($code);
            $pays->setContinent($continent);
            $pays->setRegion($region);
            $pays->setSurfaceArea($surface);
            $pays->setIndepYear($inde);
            $pays->setLifeExpectancy($esperance);
            $pays->setGNP($gnp);
            $pays->setGNPOld($gnpold);
            $pays->setLocalName($local);
            $pays->setGovernmentForm($gov);
            $pays->setHeadOfState($head);
            $pays->setCapital($capital);
            $pays->setPopulation($pop);
            $pays->setCode2($code2);

             $_SESSION['flash'] = "Pays ajouté";
            $this->daocountry->save($pays);
            $url = $_SERVER['HTTP_REFERER'];
            header('Location:'.$url);
        }
        else
        {
            $_SESSION['flash'] = "Erreur durant l'ajout du pays";
            $url = $_SERVER['HTTP_REFERER'];
            header('Location:'.$url);
        }
    }
    
    /**
     * 
     * @param type $id id du pays à modifier
     * Affiche un formulaire prérempli avec le pays à modifier
     */
    public function edit($id)
    {
        
        
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        
        if (Auth::isLogged() && Auth::isAllowed(Auth::$CANUPDATE)){
            $csrf_token = $this->generateToken();
           $pays = $this->daocountry->find($id);
           $page = renderer::render('editcountry.php',compact('pays','csrf_token'));
           echo $page;
        }
    }
    
    
    /**
     * Modifie le paye avec les infos transmises par formulaire
     */
    public function doEdit()
    {
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        if ($this->check('csrf_token') && Auth::isLogged() && Auth::isAllowed(Auth::$CANUPDATE))
        {
            
            
            $nom = htmlspecialchars($_POST['Name']);
            $code = htmlspecialchars($_POST['Code']);
            $continent = htmlspecialchars($_POST['Continent']);
            $region = htmlspecialchars($_POST['Region']);
            $surface = htmlspecialchars($_POST['SurfaceArea']);
            $inde = htmlspecialchars($_POST['IndepYear']);
            $esperance = htmlspecialchars($_POST['LifeExpectancy']);
            $gnp = htmlspecialchars($_POST['GNP']);
            $gnpold = htmlspecialchars($_POST['GNPOld']);
            $local = htmlspecialchars($_POST['LocalName']);
            $gov = htmlspecialchars($_POST['GovernmentForm']);
            $head = htmlspecialchars($_POST['HeadOfState']);
            $capital = htmlspecialchars($_POST['Capital']);
            $pop = htmlspecialchars($_POST['Population']);
            $code2 = htmlspecialchars($_POST['Code2']);
            $id = htmlspecialchars($_POST['Country_Id']);
            $pays = new Country();
            $pays->setName($nom);
            $pays->setCode($code);
            $pays->setContinent($continent);
            $pays->setRegion($region);
            $pays->setSurfaceArea($surface);
            $pays->setIndepYear($inde);
            $pays->setLifeExpectancy($esperance);
            $pays->setGNP($gnp);
            $pays->setGNPOld($gnpold);
            $pays->setLocalName($local);
            $pays->setGovernmentForm($gov);
            $pays->setHeadOfState($head);
            $pays->setCapital($capital);
            $pays->setPopulation($pop);
            $pays->setCode2($code2);
            $pays->setCountry_Id($id);


            $this->daocountry->update($pays);
            $_SESSION['flash'] = "Modification réussie";
            $url = $_SERVER['HTTP_REFERER'];
            header('Location:'.$url);
        }
        else
        {
            $_SESSION['flash'] = "Erreur durant la modification du pays";
            $url = $_SERVER['HTTP_REFERER'];
            header('Location:'.$url);
        }
    }
    
    /**
     * 
     * @param type $id id du pays à supprimer
     * Supprime le pays
     */
    public function delete($id)
    {
        
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        /** @var City $ville */
        if (Auth::isLogged() && Auth::isAllowed(Auth::$CANDELETE)){
            $this->daocountry->remove($id);
            $url = $_SERVER['HTTP_REFERER'];
            header('Location:'.$url);
        }

    }
}
