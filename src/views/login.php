<?php 
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
?>
  <head>
  	  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  	

                <link href="/css/stylePays.css" rel="stylesheet" type="text/css"/>
  		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

	  	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

	  	
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
    <header>
	  	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="/">
		  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Globe_icon.svg/1200px-Globe_icon.svg.png" alt="">
		World Data</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
		    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		      <li class="nav-item active">
		        <a class="nav-link" href="/">Continent <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="/country">Pays</a>
		      </li>
		    </ul>
		    <form class="form-inline my-2 my-lg-0">
		      <input class="form-control mr-sm-2" type="search" placeholder="Rechercher">
		      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
		    </form>
                     <div class="login">
                          <p>
                              <?php if (Auth::isLogged()){
                                  ?>
                                    <form class="form-inline" action="/logout" method="GET">
                                        <button class="btn btn-outline-danger" type="submit"> <?php echo $_SESSION[Auth::$KEY]->getNom()?><br>Déconnexion</button>
                                    </form>
                            <?php
                                
                              }
                              else {?>
                                    <form class="form-inline" action="/login" method="GET">
                                        <button class="btn btn-outline-info" type="submit">Connexion</button>
                                    </form>
                              <?php
                              }
                              ?>
                          </p>
                      </div>
		  </div>
		</nav>
  </header>
    <body>

        <center>
        <?php if (isset($_SESSION['flash']))
        {
        ?> 
        <div class="error" style="margin-top:10px;">
            <h3><span class="badge badge-warning"><?php echo $_SESSION['flash']?></span><h3>
        </div>
        
        <?php
        unset($_SESSION['flash']);
        }
        ?>
            
        <form method="POST" action="/login">
            <div class="container-fluid">
            <div class="container">
                
            <div class="form-group">
              <label for="exampleInputEmail1">Login</label>
              <input type="email" class="form-control" aria-describedby="emailHelp" name="login" style="width:300px; height:40px;">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" name="password"  style="width:300px; height:40px;">
            </div>
                
            <input type="hidden" class="form-control" name = "csrf_token"  id ="csrf_token" value = "<?php echo $csrf_token ?>">
            
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </div>
          </form>
        </center>
        
    

    </body>
    
</html>