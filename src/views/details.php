<?php 
require_once '../src/utils/Auth.php'; 

if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
}
?>
<!DOCTYPE html>
<html>
 
  <head>
  	  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  	
                <link href="/css/styleDetails.css" rel="stylesheet" type="text/css"/>

  		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

	  	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

	  	
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <header>
  	
	  	<nav class="navbar navbar-expand-lg navbar-light bg-light">

		  <a class="navbar-brand" href="/">
		  	<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Globe_icon.svg/1200px-Globe_icon.svg.png" alt="">
		  World Data
			</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
		    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

		      <li class="nav-item">
		        <a class="nav-link" href="/">Continent</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="/country">Pays</a>
		      </li>
		    </ul>
		    <form class="form-inline my-2 my-lg-0">
		      <input class="form-control mr-sm-2" type="search" placeholder="Rechercher">
		      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
		    </form>
                    <div class="login">
                          <p>
                            <?php if (Auth::isLogged()){
                                  ?>
                                    <form class="form-inline" action="/logout" method="GET">
                                        <button class="btn btn-outline-danger" type="submit"> <?php echo $_SESSION[Auth::$KEY]->getNom()?><br>Déconnexion</button>
                                    </form>
                            <?php
                                
                              }
                              else {?>
                                    <form class="form-inline" action="/login" method="GET">
                                        <button class="btn btn-outline-info" type="submit">Connexion</button>
                                      </form>
                              <?php
                              }
                              ?>
                          </p>
                      </div>
		  </div>
		</nav>
  </header>
  <body>



  	<div id = "main">

            <h1>Details pour le pays <?php echo $country->getName()?></h1>
  		<div class = "drapeau"><img src="<?php echo $country->getImage2() ?>"  width="10%" height="10%" alt="Red dot"></div>
  		<table class="table">
		  <tbody>
		    <tr>
		      <th scope="row">Id</th>
                      
		      <td><?php /** @var Country $country*/
                      echo $country->getCountry_Id() ?></td>
		    </tr>
		    <tr>
		      <th scope="row">Nom</th>
                      <td><?php echo $country->getName()?></td>
		    </tr>
		    <tr>
		      <th scope="row">Continent</th>
		      <td><?php echo $country->getContinent()?></td>
		    </tr>
		    <tr>
		      <th scope="row">Capitale</th>
		      <td><?php echo $country->getCapital()?></td>
		    </tr>
		    <tr>
		      <th scope="row">Population</th>
		      <td><?php echo $country->getPopulation()?></td>
		    </tr>
		  </tbody>
		</table>

		<table class="table table-hover">
		  <thead class="thead-dark">
		    <tr>
		      <th scope="col">Id</th>
		      <th scope="col">Nom</th>
		      <th scope="col">District</th>
		      <th scope="col">Population</th>
                      <?php if (Auth::isLogged() && (Auth::hasRole('admin') || Auth::hasRole('dev') )){?>
		      <th scope="col"><span class="fa fa-edit"></th>
                      <?php }?>
		    </tr>
		  </thead>
		  <tbody>
                      
                    <?php for ($i = 0; $i < count($cities); $i++) {
                        /** @var City $city*/
                        $city = $cities[$i];                    
                           
                       ?>
		    <tr>
                      <td><?php echo $city->getCity_Id()?></td>
                      <td>  <?php echo $city->getName()?> </td>
                      <td><?php echo $city->getDistrict()?></td>
                      <td><?php echo $city->getPopulation()?></td>
                      <?php if (Auth::isLogged() && (Auth::hasRole('admin') || Auth::hasRole('dev'))){?>
                      <td> 
                          
                           
                          <?php if (Auth::isAllowed(Auth::$CANUPDATE)){ ?> <a href="<?php echo '/city/edit/'.$city->getCity_Id() ?>"> <span class="fa fa-pen">  </span> </a> <?php }?>
                          <?php if (Auth::isAllowed(Auth::$CANDELETE)){?> <a href="<?php echo '/city/del/'.$city->getCity_Id() ?>"><span class="fa fa-trash"></span></a> <?php }?> 
                      </td>
                      <?php } ?>
		    </tr>
                    
                    <?php } ?>

		  </tbody>
		</table>

        <div class="pagination">
            <?php echo $pagination ?>
        </div>
  	</div>
  </body>
</html>