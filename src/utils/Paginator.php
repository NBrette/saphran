<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Paginator
 *
 * @author nicolas
 */
trait Paginator {
    
    /**
     * 
     * @param string $uri   base de l'url
     * @param int $page     page courante
     * @param int $total_items      nombre d'item total
     * @param int $items_per_page   nombre d'item à afficher par page (par défaut 10)
     */
    public function paginate(string $uri, int$page, int $total_items, int $items_per_page)
    {
     
      $nbOfPage  = ceil($total_items/$items_per_page) ;
      
      $previousState = 'active';
      $nextState = 'active';
      
      //Vérfication si dernière ou première page
      if ($page == 1) $previousState = 'disabled';
      if ($page == $nbOfPage) $nextState = 'disabled';
      $previousPage = $page-1;
      $nextPage = $page+1;

      $HTML =        <<<XXX
                        
                        <nav aria-label="Page navigation example">
                          <ul class="pagination">
                            <li class="page-item $previousState "> <a class="page-link" href="{$uri}?p={$previousPage}&items={$items_per_page}">Previous</a></li>
                      XXX;
              
        for ($i = 1 ; $i<= $nbOfPage; $i++)
            {
                

                $url = $uri.  '?p='. $i . '&items='.$items_per_page;
                
                if( $page == $i)
                {
                        $HTML .= <<<XXX
                                <li class="page-item active" ><a class="page-link" href= "$url" >$i </a></li>
                        XXX;
                }
                else
                {
                        $HTML .= <<<XXX
                                <li class="page-item" ><a class="page-link" href= "$url" >$i </a></li>
                        XXX;
                }

            }
        $HTML .= <<<XXX

                        <li class="page-item $nextState "><a class="page-link" href="{$uri}?p={$nextPage}&items={$items_per_page}" >Next</a></li>
                      </ul>
                  </nav>
                XXX;
        
        
        return $HTML;
    
      
              
    }
}
