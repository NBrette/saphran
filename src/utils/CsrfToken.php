<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CsrfToken
 *
 * @author student
 */
trait CsrfToken{
    private static $token_name = 'csrf_token';
    
    public function generateToken() : string 
    {
        
        if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        }
        
        $token = md5(uniqid(microtime(), true));  
        $_SESSION[self::$token_name] = $token;
        
        return $token;
        //return $_SESSION[self::$token_name] ;
    }
    
    public function check(string $key) : bool
    {
        if ($_SESSION[$key] == $_POST[$key] )
        {
            return true;
        }
        else
        {
            $_SESSION[self::$token_name] = null;
            return false;
        }
    }
}
