<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Filter
 *
 * @author student
 */
class Filter {
    
    private $visitors = [];
    private $data;
    
    public function __construct( array $formData)
    {
        $this->data = $formData;
    }
    
    public function acceptVisitor(string $key, AbstractVisitor $visitor)
    {
        $this->visitors[$key] = $visitor;
    }
    
    public function visit()
    {
        $keys = array_keys($this->visitors);
        
        foreach ($keys as $key) {
            
            if (!isset($this->data[$key]) Or !$this->visitors[$key]->visite($this->data[$key])) return false;
            
        }
        
        return true;
    }
}
