<?php
require_once 'AbstractVisitor.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of PasswordVisitor
 *
 * @author student
 */
class PasswordVisitor extends AbstractVisitor{
    //put your code here
    public function visite(string $data): bool {
        
        $uppercase = preg_match('@[A-Z]@', $data);
        $lowercase = preg_match('@[a-z]@', $data);
        $number = preg_match('@[0-9]@', $data);
        $specialCaracter = preg_match('/[?!#=+]/', $data);
        
        
        if (strlen($data) > 5 && $uppercase && $lowercase && $number && $specialCaracter) return true; else return false;

    }

}
