<?php

require_once 'SingletonConfigReader.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SingletonDatabase
 *
 * @author nicolas
 */
class SingletonDatabase {
    
    /** @var PDO $cnx*/
    public $cnx;
    private static $serveur;
    private static $database;
    private static $username;
    private static $password;
    
    /** @var SingletonDatabase $instance*/
    public static $instance = null;
    
    
    private function __construct()
    {
        
      
      self::$serveur = SingletonConfigReader::getInstance()->getValue('serveur','mariadb');
      self::$database = SingletonConfigReader::getInstance()->getValue('database','mariadb');
      self::$username = SingletonConfigReader::getInstance()->getValue('username','mariadb');
      self::$password = SingletonConfigReader::getInstance()->getValue('password','mariadb');

      $this->cnx = new PDO('mysql:host='. self::$serveur.';dbname='.self::$database ,self::$username,self::$password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
      $this->cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $this->cnx->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }
    
    public static function getInstance() : SingletonDatabase
    {
        if (self::$instance == null)
        {
            self::$instance = new SingletonDatabase();
        }
        return self::$instance;
    }
    
    

}
