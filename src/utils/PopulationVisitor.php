<?php
require_once 'AbstractVisitor.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PopulationVisitor
 *
 * @author student
 */
class PopulationVisitor extends AbstractVisitor{
    
    public function visite(string $data): bool {
        
        $isInt = ctype_digit($data);

        if (strlen($data) > 0 && strlen($data) < 11 && $isInt) return true; else return false;

    }
}
