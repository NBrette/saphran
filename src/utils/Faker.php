#!/bin/php


<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Faker
 *
 * @author student
 */
class Faker {
    
    private $cnx;
    private $config;
    
    public function __construct() {
        $this->config = parse_ini_file(filter_input(INPUT_SERVER,'DOCUMENT_ROOT').'../../config.ini', true);
        $this->cnx = new PDO('mysql:host='. $this->config['mariadb']['serveur'].';dbname='.$this->config['mariadb']['database'] ,$this->config['mariadb']['username'],$this->config['mariadb']['password'], array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    }
    
    public function insertRole()
    {
        
        $sql = "INSERT INTO roles VALUES (1,'admin','CRUD') ;";
        $sql2 = "INSERT INTO roles VALUES (2,'dev','CRU') ;";
        $sql3 = "INSERT INTO roles VALUES (3,'testeur','R') ;";
        
        $requetes = [$sql, $sql2,$sql3];
        
        foreach ($requetes as $requete) {
            
            $preparedStatement =$this->cnx->prepare($requete);
            $preparedStatement->execute();
        }

    }
    
    public function insertUser()
    {
        
        
        
        $sql = "INSERT INTO utilisateur VALUES (1,'admin@world.com', :password,'administrateur',1) ;";
        $sql2 = "INSERT INTO utilisateur VALUES (2,'dev@world.com', :password,'Bernardo',2) ;";
        $sql3 = "INSERT INTO utilisateur VALUES (3,'testeur@world.com',:password,'administrateur',1) ;";
        
        $requetes = [$sql, $sql2,$sql3];
        
        foreach ($requetes as $requete) {
            
            $preparedStatement =$this->cnx->prepare($requete);
            
            $hash = password_hash('123+Aze', PASSWORD_DEFAULT);
            
            $preparedStatement->bindParam('password',$hash);
            $preparedStatement->execute();
        }
    }
}

$faker = new Faker();
$faker->insertRole();
$faker->insertUser();







