<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of Auth
 *
 * @author student
 */
class Auth {
    
    static $KEY = "USERSESSION";
    static $CANCREATE = 'C';
    static $CANREAD = 'R';
    static $CANUPDATE = 'U';
    static $CANDELETE = 'D';
    
    /**
     * Met l'objet user en session
     * @param type $user
     * 
     */
    public static function login($user)
    {   
        $_SESSION[self::$KEY] = $user;
        
    }
    
    /**
     * Deconnecte l'utilisateur
     */
    public static function logout()
    {
        if (isset($_SESSION[self::$KEY]))
        {
            unset($_SESSION[self::$KEY]);
            //$_SESSION[self::$KEY] = null;
        }
    }
    
    /**
     * Vérifie si un utilisateur est connecté
     * @return bool
     */
    public static function isLogged() : bool
    {
        if (isset($_SESSION[self::$KEY]))
        {
            return true;
        }
        return false;
    }
    
    /**
     * Verifie que l'utilisateur a le role nécessaire 
     * @return bool
     */
    public static function hasRole(string $role) : bool
    {
        if ( strtolower($role) == strtolower($_SESSION[self::$KEY]->getRole()->getNom()))
        {
            return true;
        }
        return false;
    }
    
    /*
     * Verifie que l'utilisateur a les permissions nécessaires 
     */
    public static function isAllowed(string $perm) : bool
    {       
        if(strpos(strtolower($_SESSION[self::$KEY]->getRole()->getPermission()), strtolower($perm))!== False){
            return true;
        }
        
        return false;
           
    }
}



