<?php
require_once 'City.php';
require_once 'Country.php';
require_once 'DAO.php';


/**
 * Description of DAOCity
 *
 * @author nicolas
 */
class DAOCity extends DAO {
    
    
    public function __construct($cnx) 
    {
        parent::__construct($cnx);
   
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function find($id)
    {
        $sql = "SELECT * FROM city WHERE City_Id=:id";
        $preparedStatement =$this->cnx->prepare($sql);
        $preparedStatement->bindParam('id',$id);
        $preparedStatement->execute();
        $city = $preparedStatement->fetchObject('City');
        
        return $city;
    }
    
    
    
    /**
     * 
     * @param City $entity
     * @return void
     */
    public function save($entity) : void
    {
        $name = $entity->getName();
        $countryCode = $entity->getCountryCode();
        $district = $entity->getDistrict();
        $population = $entity->getPopulation();
        
        $sql = "INSERT INTO city (Name, CountryCode, District, Population) VALUES (:Name,:CountryCode,:District,:Population)";
        $preparedStatement = $this->cnx ->prepare($sql);
        
        /**@var City $entity*/
        $preparedStatement->bindParam('Name',$name);
        $preparedStatement->bindParam('CountryCode',$countryCode);
        $preparedStatement->bindParam('District',$district);
        $preparedStatement->bindParam('Population',$population);
        $preparedStatement->execute();
    }
    
    /**
     * 
     * @param type $entity
     * @return void
     */
    public function update($entity) : void
    {
        $sql = "UPDATE city SET Name =:name,CountryCode=:countryCode,District =:district, Population = :population WHERE City_Id = :id;";
        
        $preparedStatement = $this->cnx ->prepare($sql);
        
        $id = $entity-> getCity_Id();
        $name = $entity->getName();
        $countryCode = $entity->getCountryCode();
        $district = $entity->getDistrict();
        $population = $entity->getPopulation();
        
        $preparedStatement->bindParam('name',$name);
        $preparedStatement->bindParam('countryCode',$countryCode);
        $preparedStatement->bindParam('district',$district);
        $preparedStatement->bindParam('population',$population);
        $preparedStatement->bindParam('id',$id);
        
        $preparedStatement->execute();

    }
    
    public function remove($entity) : void
    {
        $id = $entity->getCity_Id();
        $sql = "DELETE FROM city WHERE City_Id=:id;";
        $preparedStatement = $this->cnx ->prepare($sql);
        /**@var City $entity*/
        $id = $entity->getCity_Id();
        $preparedStatement->bindParam('id',$id);
        $preparedStatement->execute();
    }
    
    public function findAll($limitStart,$limitEnd) : array
    {
        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM city LIMIT :itemsPerPage OFFSET :limitStart;";
        $preparedStatement = $this->cnx ->prepare($sql);
        $preparedStatement->bindParam('itemsPerPage',$itemsPerPage);
        $preparedStatement->bindParam('limitStart',$limitStart);
        $preparedStatement->execute();
        $values = [];
        while (($value = $preparedStatement->fetchObject('City'))!= False)
        {
            array_push($values, $value);
        }
        
        return $values;

    }
    
    public function count() : int
    {
        $sql = "SELECT COUNT(City_Id) FROM city;";
        $preparedStatement =$this->cnx->prepare($sql);
        $preparedStatement->execute();
        $city = $preparedStatement->fetch();
        
        return $city[0];
    }
    
    public function findCitiesByCountryCode($countryCode,$itemsPerPage,$limitStart)
    {
        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM city WHERE CountryCode = :code LIMIT :itemsPerPage OFFSET :limitStart;";
        $preparedStatement = $this->cnx ->prepare($sql);        
        $preparedStatement->bindParam('code',$countryCode);
        $preparedStatement->bindParam('itemsPerPage',$itemsPerPage);
        $preparedStatement->bindParam('limitStart',$limitStart);
        $preparedStatement->execute();
        $values = [];
        while (($value = $preparedStatement->fetchObject('City'))!= False)
        {
            array_push($values, $value);
        }
        
        return $values;
    }
    
    public function foundRows()
    {
        $totalRows = $this->cnx->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
        
        return $totalRows;
    }
    

}
