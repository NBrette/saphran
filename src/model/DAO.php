<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DAO
 *
 * @author student
 */
abstract class DAO{
    
    protected $cnx;
    
    public function __construct($cnx) 
    {
        $this->cnx = $cnx;
        
    }
    
    abstract public function find($id);
    
    abstract public function save($entity) : void;
    
    abstract public function update($entity) :void;

    abstract public function remove($entity) : void;

    abstract public function findAll($limitStart, $limitEnd) : array;

    
    abstract public function count() : int;

    
    
    
    
}
