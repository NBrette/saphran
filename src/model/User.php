<?php

require_once 'model.php';

class User extends Model
{
    
    protected $id;
    protected $login;
    protected $password;
    protected $nom;
    protected $idrole;
    protected $role;
    
    /**
     * 
     * @param array $data
     */
    public function __construct( array $data=null  )
    {
        parent::__construct();
        
        if ($data != null)
        {
            $this->id = $data["Id"];
            $this->login = $data["Login"];
            $this->password = $data["Password"];
            $this-> nom = $data["Nom"];
            $this-> idrole = $data["IdRole"];
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getLogin() {
        return $this->login;
    }

    function getPassword() {
        return $this->password;
    }

    function getNom() {
        return $this->nom;
    }

    function getIdRole() {
        return $this->idrole;
    }

    function setId($Id): void {
        $this->id = $Id;
    }

    function setLogin($Login): void {
        $this->login = $Login;
    }

    function setPassword($Password): void {
        $this->password = $Password;
    }

    function setNom($Nom): void {
        $this->nom = $Nom;
    }

    function setIdRole($IdRole): void {
        $this->idrole = $IdRole;
    }

    function getRole() {
        return $this->role;
    }

    function setRole($role): void {
        $this->role = $role;
    }



    
}