<?php

require_once 'City.php';
require_once 'DAO.php';
require_once 'DAOCity.php';
require_once 'Country.php';



/**
 * Description of DAOCountry
 *
 * @author student
 */
class DAOCountry extends DAO {
    //put your code here
    public function __construct($con) 
    {
        parent::__construct($con);
    }
  
   
    public function find($id) : Country {
        $sql = "SELECT * FROM country WHERE Country_Id =:id";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("id", $id);
        $preparedStatement->execute();
        $country = $preparedStatement->fetchObject('country');
        
        return $country;
    }
    public function save($entity) : void{
        
/**@var Country $entity*/
        $name = $entity->getName();
        $code = $entity->getCode();
        $population = $entity->getPopulation();
        $gnpold = $entity->getGNPOld();
        $code2 = $entity->getCode2();
        $continent = $entity->getContinent();
        $gov = $entity->getGovernmentForm();
        $head = $entity->getHeadOfState();
        $indep = $entity->getIndepYear();
        $local = $entity->getLocalName();
        $esperance = $entity->getLifeExpectancy();
        $region = $entity->getRegion();
        $surface = $entity->getSurfaceArea();
        $gnp = $entity->getGNP();
        
        
        $sql = "INSERT INTO country (Code,Name,Continent,Region,SurfaceArea,IndepYear,Population,LifeExpectancy,GNP,GNPOld,LocalName,GovernmentForm,HeadOfState,Capital,Code2) "
                . "VALUES (:Code,:Name,:Continent,:Region,:SurfaceArea,:IndepYear,:Population,:LifeExpectancy,:GNP,:GNPOld,:LocalName,:GovernmentForm,:HeadOfState,:Capital,:Code2)";
        $preparedStatement = $this->cnx ->prepare($sql);
        var_dump($preparedStatement);
        
        
        var_dump($preparedStatement->bindParam('Code',$code));
        var_dump($preparedStatement->bindParam('Name',$name));
        var_dump($preparedStatement->bindParam('Continent',$continent));
        var_dump($preparedStatement->bindParam('SurfaceArea',$surface));
        var_dump($preparedStatement->bindParam('IndepYear',$indep));
        var_dump($preparedStatement->bindParam('Population',$population));
        var_dump($preparedStatement->bindParam('LifeExpectancy',$esperance));
        var_dump($preparedStatement->bindParam('GNP',$gnp));
        var_dump($preparedStatement->bindParam('GNPOld',$gnpold));
        var_dump($preparedStatement->bindParam('LocalName',$local));
        var_dump($preparedStatement->bindParam('GovernmentForm',$gov));
        var_dump($preparedStatement->bindParam('HeadOfState',$head));
        var_dump($preparedStatement->bindParam('Capital',$population));
        var_dump($preparedStatement->bindParam('Code2',$code2));
        var_dump($preparedStatement->bindParam('Region',$region));
        var_dump($preparedStatement->execute());
            
    }

    public function update($entity) : void{
        /**@var Country $entity*/
        $id = $entity->getCountry_Id();
        $name = $entity->getName();
        $code = $entity->getCode();
        $population = $entity->getPopulation();
        $gnpold = $entity->getGNPOld();
        $code2 = $entity->getCode2();
        $continent = $entity->getContinent();
        $gov = $entity->getGovernmentForm();
        $head = $entity->getHeadOfState();
        $indep = $entity->getIndepYear();
        $local = $entity->getLocalName();
        $esperance = $entity->getLifeExpectancy();
        $region = $entity->getRegion();
        $surface = $entity->getSurfaceArea();
        $gnp = $entity->getGNP();
        
        $sql =  "UPDATE country SET Code = :Code , Name = :Name , Continent = :Continent, Region = :Region , SurfaceArea = :SurfaceArea , IndepYear = :IndepYear , Population = :Population , " 
        . " LifeExpectancy = :LifeExpectancy , GNP = :GNP, GNPOld = :GNPOld , LocalName=:LocalName , GovernmentForm=:GovernmentForm ,HeadOfState=:HeadOfState , Capital=:Capital, Code2=:Code2 WHERE Country_Id=:Country_Id";
        
        $preparedStatement = $this->cnx ->prepare($sql);
        var_dump($preparedStatement);
        
        
        $preparedStatement->bindParam('Country_Id',$id);
        $preparedStatement->bindParam('Code',$code);
        $preparedStatement->bindParam('Name',$name);
        $preparedStatement->bindParam('Continent',$continent);
        $preparedStatement->bindParam('SurfaceArea',$surface);
        $preparedStatement->bindParam('IndepYear',$indep);
        $preparedStatement->bindParam('Population',$population);
        $preparedStatement->bindParam('LifeExpectancy',$esperance);
        $preparedStatement->bindParam('GNP',$gnp);
        $preparedStatement->bindParam('GNPOld',$gnpold);
        $preparedStatement->bindParam('LocalName',$local);
        $preparedStatement->bindParam('GovernmentForm',$gov);
        $preparedStatement->bindParam('HeadOfState',$head);
        $preparedStatement->bindParam('Capital',$population);
        $preparedStatement->bindParam('Code2',$code2);
        $preparedStatement->bindParam('Region',$region);
        var_dump($preparedStatement->execute());
    }

    public function remove($id) : void{
        $sqlCity = "DELETE FROM city WHERE CountryCode = (SELECT Code FROM country WHERE Country_Id = :Country_Id)";
        $prepareStatementCity = $this->cnx->prepare($sqlCity);
        $prepareStatementCity->bindParam("Country_Id", $id);
        
        $codeCity = $prepareStatementCity->execute();

        $sqlLanguage = "DELETE FROM countrylanguage WHERE CountryCode = (SELECT Code FROM country WHERE Country_Id = :Country_Id)";
        $prepareStatementLanguage = $this->cnx->prepare($sqlLanguage);
        $prepareStatementLanguage->bindValue("Country_Id", $id);
        $codeLanguage = $prepareStatementLanguage->execute();

        $sqlCountry = "DELETE FROM country WHERE Country_Id = :Country_Id";
        $prepareStatementCountry = $this->cnx->prepare($sqlCountry);
        $prepareStatementCountry->bindValue("Country_Id", $id);
        $codeCountry = $prepareStatementCountry->execute();
    }
        
        
        
    

 
    public function findAll($itemsPerPage,$limitStart) : array{
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM country LIMIT :itemsPerPage OFFSET :limitStart ;";
        
        $preparedStatement = $this->cnx ->prepare($sql);
        $preparedStatement->bindParam('itemsPerPage',$itemsPerPage);
        $preparedStatement->bindParam('limitStart',$limitStart);
        $preparedStatement->execute();
        
        $values = [];
        while (($value = $preparedStatement->fetchObject('Country'))!= False)
        {
            array_push($values, $value);
        }
        
        return $values;
    }

    public function count() : int {
        $sql = "SELECT COUNT(Country_Id) FROM country";
        $preparedStatement = $this->cnx->prepare($sql);
        
        $preparedStatement->execute();
        $country = $preparedStatement->fetch();
        
        return $country[0];
    }
    


    public function get_enum_from_continent()
    {
        $sql = "SELECT COLUMN_TYPE FROM information_schema.`COLUMNS` WHERE TABLE_NAME = 'country' AND COLUMN_NAME = 'Continent';";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->execute();
        $result = $preparedStatement->fetch();
        
        //mise en forme pour transformer l'enum en liste
        $continent = str_replace(['enum(',')',"'"],'' , $result[0]);
        $continent = explode(',', $continent);
    
        return $continent;
        
    }
    
    public function findAllByContinent($continent,$itemsPerPage,$limitStart) : array{
        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM country WHERE Continent =:continent LIMIT :itemsPerPage OFFSET :limitStart ;";
        $preparedStatement = $this->cnx ->prepare($sql);
        $preparedStatement->bindParam('continent',$continent);
        $preparedStatement->bindParam('itemsPerPage',$itemsPerPage);
        $preparedStatement->bindParam('limitStart',$limitStart);
        
        $preparedStatement->execute();
        $values = [];
        while (($value = $preparedStatement->fetchObject('Country'))!= False)
        {
            array_push($values, $value);
        }
        
        return $values;
    }
    
    
    public function findCountryResearch($research)
    {
        $sql = "SELECT * FROM country WHERE Name LIKE CONCAT('%', :research, '%');";
        $preparedStatement = $this->cnx ->prepare($sql);
        $preparedStatement->bindParam('research',$research);
        $preparedStatement->execute();
        $values = [];
        while (($value = $preparedStatement->fetchObject('Country'))!= False)
        {
            array_push($values, $value);
        }
        
        return $values;
    }
    
    public function foundRows()
    {
        $totalRows = $this->cnx->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
        
        return $totalRows;
    }
    
    
    
    
}
