<?php

require_once 'model.php';

class City extends Model
{
    
    protected $City_Id;
    protected $Name;
    protected $CountryCode;
    protected $District;
    protected $Population;
    
    /**
     * 
     * @param array $data
     */
    public function __construct( array $data=null  )
    {
        parent::__construct();
        
        if ($data != null)
        {
            $this->City_Id = $data["City_Id"];
            $this->Name = $data["Name"];
            $this->CountryCode = $data["CountryCode"];
            $this ->District = $data["District"];
            $this-> Population = $data["Population"];
        }

    
    }
    function getCity_Id() {
        return $this->City_Id;
    }

    function getName() {
        return $this->Name;
    }

    function getCountryCode() {
        return $this->CountryCode;
    }


    function setName($Name): void {
        $this->Name = $Name;
    }


    function getDistrict() {
        return $this->District;
    }

    function getPopulation() {
        return $this->Population;
    }

    function setDistrict($District): void {
        $this->District = $District;
    }

    function setPopulation($Population): void {
        $this->Population = $Population;
    }

    function setCountryCode($CountryCode): void {
        $this->CountryCode = $CountryCode;
    }


    
}