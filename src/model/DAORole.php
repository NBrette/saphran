<?php
require_once 'DAO.php';
require_once 'User.php';
require_once 'Role.php';


/**
 * Description of DAOCity
 *
 * @author nicolas
 */
class DAORole extends DAO {
    
    
    public function __construct($cnx) 
    {
        parent::__construct($cnx);
   
    }

    public function count(): int {
        
    }

    public function find($id) : Role {
        $sql = "SELECT * FROM roles WHERE idRole=:id";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("id", $id);
        $preparedStatement->execute();
        $role = $preparedStatement->fetchObject('role');
        
        return $role;
    }

    public function findAll($limitStart, $limitEnd): array {
        
    }

    public function remove($entity): void {
        
    }

    public function save($entity): void {
        
    }

    public function update($entity): void {
        
    }

}