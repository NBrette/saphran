<?php

require_once 'model.php';

class Role extends Model
{
    
    protected $idRole;
    protected $nom;
    protected $permission;

    
    /**
     * 
     * @param array $data
     */
    public function __construct( array $data=null  )
    {
        parent::__construct();
        
        if ($data != null)
        {
            $this->idRole = $data["IdRole"];
            $this->nom = $data["Nom"];
            $this->permission = $data["Permission"];

        }
    }
    
    function getIdRole() {
        return $this->idRole;
    }

    function getNom() {
        return $this->nom;
    }

    function getPermission() {
        return $this->permission;
    }

    function setIdRole($IdRole): void {
        $this->idRole = $IdRole;
    }

    function setNom($Nom): void {
        $this->nom = $Nom;
    }

    function setPermission($Permission): void {
        $this->permission = $Permission;
    }


}