<?php
require_once 'DAO.php';
require_once 'User.php';


/**
 * Description of DAOCity
 *
 * @author nicolas
 */
class DAOUser extends DAO {
    
    
    public function __construct($cnx) 
    {
        parent::__construct($cnx);
   
    }

    public function count(): int {
        
    }

    public function find($login,$password=null) {
        $sql = "SELECT * FROM utilisateur WHERE login=:login";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindParam("login", $login);
        $preparedStatement->execute();
        $user = $preparedStatement->fetchObject('User');

        if ($user !=null && password_verify($password,$user->getPassword()))
        {
            return $user;
        }
        else 
        {
            return null;
        }

        
        
    }

    public function findAll($limitStart, $limitEnd): array {
        
    }

    public function remove($entity): void {
        
    }
    
    /**@var User $entity*/
    public function save($entity): void {
        $login = $entity->getLogin();
        $password = $entity->getPassword();
        $nom = $entity->getNom();
        $idrole = $entity->getIdRole();
        
        $sql = "INSERT INTO utilisateur (login, password, nom, idrole) VALUES (:login,:password,:nom,:idrole)";
        $preparedStatement = $this->cnx ->prepare($sql);
        
        /**@var User $entity*/
        $preparedStatement->bindParam('login',$login);
        $preparedStatement->bindParam('password',$password);
        $preparedStatement->bindParam('nom',$nom);
        $preparedStatement->bindParam('idrole',$idrole);
        $preparedStatement->execute();
    }

    public function update($entity): void {
        
    }
    


}